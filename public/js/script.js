	// create the module and name it scotchApp
	var scotchApp = angular.module('scotchApp', ['ngRoute']);

	// configure our routes
	scotchApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'home.html',
				controller  : 'mainController'
			})
			.when('/#/', {
				templateUrl : 'home.html',
				controller  : 'mainController'
			})
			.when('/showProducts', {
				templateUrl : 'home.html',
				controller  : 'mainController'
			})

			// route for the catalog page
			.when('/showProducts/:type', {
				templateUrl : 'catalog.html',
				controller  : 'productListController'
			})

			// route for the product page
			.when('/showProduct/:id', {
				templateUrl : 'product.html',
				controller  : 'productController'
			});
	});

	// create the controller and inject Angular's $scope
	scotchApp.controller('mainController', function($scope,$http) {
		$scope.catalogObjects = [{"name": "sunglass", "value": []}, {"name": "books", "value": []}, {"name": "paintings", "value": []}];
		$http.get("/store/products")
	    .success(function(response) {
	    	console.log("catalogs -  ",response);
	    	for(var i=0;i<response.length;i++) {
	    		var tempObject = response[i];
	    		if(tempObject.parentCategory === "sunglass") {
	    			$scope.catalogObjects[0].value.push(tempObject);
	    		}
	    		else if(tempObject.parentCategory === "books") {
	    			$scope.catalogObjects[1].value.push(tempObject);
	    		}
	    		else if(tempObject.parentCategory === "paintings") {
	    			$scope.catalogObjects[2].value.push(tempObject);
	    		}
	    	}
	    	console.log("$scope.catalogObjects -  ",$scope.catalogObjects);
	    });
	});

	scotchApp.controller('productListController', function($scope,$http, $routeParams) {
		
		$scope.parent_type = $routeParams.type;
		console.log("$scope.parent_type- ",$scope.parent_type);
		$http.get("/store/products/parentCategory/"+$scope.parent_type)
	    .success(function(response) {
	    	console.log("catalogs -  ",response);
	    	$scope.productList = response;
	    });
	});

	scotchApp.controller('productController', function($scope,$http, $routeParams) {
		$scope.id = $routeParams.id;
		console.log("$scope.parent_type- ",$scope.id);
		$http.get("/store/products/"+$scope.id)
	    .success(function(response) {
	    	console.log("catalogs -  ",response);
	    	$scope.product = response;
	    });
	});