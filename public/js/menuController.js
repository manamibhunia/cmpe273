/**
 * @author Manami
 */
function menuController($scope,$http) {
	
	$http.get("/store/catalog/")
    .success(function(response) {
    	console.log("catalogs -  ",response);
    	$scope.catalogs = response;
    });
    $scope.itemObjects = null;
    $scope.showCatalog = function(target) {
        $http.get("/store/products/parentCategory/"+target)
	    .success(function(response) {
	    	console.log("itemObjects -  ",response);
	    	$scope.itemObjects = response;
	    });
    };
}