Bonyzone
=========

CMPE273 - online shopping cart.

Version
----

1.0.0

Tech
-----------

Bonyzone uses a number of open source projects to work properly:

* [Angular.js] - angular js
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [mongo db] - mongo db
* [jQuery] - jquery 2.1.1




License
----

MIT


Start mongodb
-------------

sudo mongod

Launch mongo-console
---------------

sudo mongo
> use bonyzone_database

> show tables

> db.products.find().pretty()

> db.products.findOne({title:"Babli by Budhhadeb Guha"});

> db.products.save({title:"Abc",description:"test entry",parentCategory:"books",price:"$1234",creator:"Manami",thumbnailUrl:"babli.png"})

> db.products.update(
   { title: "Andy" },
   {
      title: "Andy",
      description: "Abc abc abc abc",
      parentCategory:"books",
      price: "$1",creator:"Manami",thumbnailUrl:"babli.png"
   },
   { upsert: true }
)

db.products.remove(
   { title: "Andy" },
   {
     justOne: true
   }
)