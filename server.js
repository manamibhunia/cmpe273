var application_root = __dirname,
    express = require("express"),
    path = require("path"),
    bodyParser = require('body-parser'),
    methodOverride = require("method-override"),
    mongoose = require('mongoose');

var app = express();

// Config

app.use(bodyParser());
app.use(methodOverride());
app.use(express.static(path.join(application_root, "public")));
app.use(function(err, req, res, next){
  console.error(err.stack);
  res.status(500).send('Something broke!'+err.stack);
});

// Database

mongoose.connect('mongodb://localhost/bonyzone_database');

var Schema = mongoose.Schema;  

app.get('/store', function (req, res) {
  res.send('BonyZone is up and running. Please use specific urls.');
});


var Product = new Schema({  
    title: { type: String, required: true },
    parentCategory: { type: String, required: true },
    description: { type: String, required: true },  
    style: { type: String, unique: false }, 
    type: { type: String, unique: false }, 
    price: { type: String, unique: true }, 
    creator: { type: String, unique: false },
    modified: { type: Date, default: Date.now },
    thumbnailUrl: { type: String, required: false }
});

var ProductModel = mongoose.model('Product', Product);  


// READ a List of Products
app.get('/store/products', function (req, res){
  return ProductModel.find(
  		function (err, products) {
		    if (!err) {
		      return res.send(products);
		    } else {
		      return console.log(err);
		    }
  		}
  );
});

//CREATE a Single Product
app.post('/store/products', function (req, res){
  var product;
  console.log("POST: ");
  console.log(req.body);
  product = new ProductModel({
    title: req.body.title,
    description: req.body.description,
    parentCategory: req.body.parentCategory,
    style: req.body.style,
    type: req.body.type,
    price: req.body.price,
    creator: req.body.creator,
    thumbnailUrl: req.body.thumbnailUrl
  });
  product.save(function (err) {
    if (!err) {
      return console.log("created");
    } else {
      return console.log(err);
    }
  });
  return res.send(product);
});

//READ a Single Product by ID
app.get('/store/products/:id', function (req, res){
  return ProductModel.findById(req.params.id, function (err, product) {
    if (!err) {
      return res.send(product);
    } else {
      return console.log(err);
    }
  });
});

//READ a Single Product by title
app.get('/store/products/title/:title', function (req, res){
	var prodTitle = req.params.title;
  return ProductModel.findOne({title: new RegExp('^'+prodTitle+'$', "i")}, function (err, product) {
    if (!err) {
      return res.send(product);
    } else {
      return console.log(err);
    }
  });
});

//READ a Single Product by parent category
app.get('/store/products/parentCategory/:parentCategory', function (req, res){
	var parentCategory = req.params.parentCategory;
  return ProductModel.find({parentCategory: new RegExp('^'+parentCategory+'$', "i")}, function (err, product) {
    if (!err) {
      return res.send(product);
    } else {
      return console.log(err);
    }
  });
});

//UPDATE a Single Product by ID
app.put('/store/products/:id', function (req, res){
  return ProductModel.findById(req.params.id, function (err, product) {
    //product.title = req.body.title;
    //product.description = req.body.description;
    //product.parentCategory = req.body.parentCategory;
    //product.style = req.body.style;
    product.thumbnailUrl = req.body.thumbnailUrl;
    return product.save(function (err) {
      if (!err) {
        console.log("updated");
      } else {
        console.log(err);
      }
      return res.send(product);
    });
  });
});

//DELETE a Single Product by ID
app.delete('/store/products/:id', function (req, res){
  return ProductModel.findById(req.params.id, function (err, product) {
    return product.remove(function (err) {
      if (!err) {
        console.log("removed");
        return res.send('');
      } else {
        console.log(err);
      }
    });
  });
});



// Launch server

var server = app.listen(8080, function() {
    console.log('Server is up and listening on port %d', server.address().port);
});